//init map
mapboxgl.accessToken =
  "pk.eyJ1IjoiamxlaWthbSIsImEiOiJjanYxMWNwazgwN28yM3lxanVlZXAzeGZ2In0.Lj-PG1lfXph58c_36qt71w";
var map = new mapboxgl.Map({
  container: "map", // container id
  style: "mapbox://styles/mapbox/streets-v11", // stylesheet location
  center: [-97.7431, 30.2672], // starting position [lng, lat]
  zoom: 12 // starting zoom
});

//init geocode
var geocoder = new MapboxGeocoder({
  countries: "us",
  accessToken: mapboxgl.accessToken,
  mapboxgl: mapboxgl
});

// boolean to check if user has submitted destination
var is_destination_set = false;
var destination = null;
var lng = null;
var lat = null;

geocoder.on("result", function(result) {
  console.log(result);
  is_destination_set = true;
  destination = result.result.place_name;
  lng = result.result.center[1];
  lat = result.result.center[0];
  // destination = result.result.properties.address;
});

geocoder.on("clear", function(result) {
  is_destination_set = false;
});

document.getElementById("geocoder").appendChild(geocoder.onAdd(map));

//form validation
function validate() {
  var fname = document.forms["form"]["fname"].value;
  var lname = document.forms["form"]["lname"].value;

  if (is_destination_set && fname != "" && lname != "") {
    sendRequest();
  } else {
    alert(
      "Woops! We could not complete your order. Please make sure all fields are filled out."
    );
  }
}

//eta request
function sendRequest() {
  var fName = document.getElementById("fname").value;
  var lName = document.getElementById("lname").value;

  console.log(destination);

  var destination_address = destination;
  console.log(destination_address);
  var mapToSend = {
    first_name: fName,
    last_name: lName,
    destination: destination_address
  };
  console.log(mapToSend);
  var jsonString = JSON.stringify(mapToSend);

  console.log(jsonString);

  var xHttpReq = new XMLHttpRequest();

  xHttpReq.open(
    "POST",
    "https://team24.demand.softwareengineeringii.com/demand/order/new",
    false
  );

  xHttpReq.setRequestHeader(
    "Content-Type",
    "application/javascript; charset=UTF-8"
  );

  xHttpReq.onreadystatechange = function() {
    if (xHttpReq.readyState == 4) {
      if (xHttpReq.status == 200) {
        response = xHttpReq.responseText;
        console.log(response);
      }
    } else {
      console.log(xHttpReq.responseText);
    }
  };

  xHttpReq.send(jsonString);

  var result = JSON.parse(response);
  console.log(result);

  // dest_lng = result.destination_longitude;
  // dest_lat = result.destination_latitude;
  eta_in_minutes = Math.floor(result.eta / 60);

  updateMap(eta_in_minutes);
}

//update map
function updateMap(eta) {
  console.log(lng);
  console.log(lat);
  // map.flyTo({
  //   center: [lng, lat]
  // });
  var popup = new mapboxgl.Popup({ closeOnClick: false })
    .setLngLat([lat, lng])
    .setHTML(
      "<h1> Your vehicle will arrive at this location in approximately " +
        eta +
        " minute(s)</h1>"
    )
    .addTo(map);
}
